// Copyright (C) 2016 The Android Open Source Project
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package {
    // See: http://go/android-license-faq
    // A large-scale-change added 'default_applicable_licenses' to import
    // all of the 'license_kinds' from "external_googletest_license"
    // to get the below license kinds:
    //   SPDX-license-identifier-BSD
    default_applicable_licenses: ["external_googletest_license"],
}

cc_defaults {
    name: "gtest_test_defaults",
    host_supported: true,
    gtest: false,
    cflags: [
        "-Wall",
        "-Werror",
        "-Wno-sign-compare",
        // because gtest_unittest.cc wants anonymous enum type.
        "-Wno-unnamed-type-template-args",
        "-Wno-unused-private-field",
    ],
    include_dirs: ["external/googletest/googletest"],
    static_libs: ["libgtest"],
    relative_install_path: "gtest_tests",
}

cc_defaults {
    name: "gtest_ndk_test_defaults",
    gtest: false,
    cflags: [
        "-Wall",
        "-Werror",
        "-Wno-sign-compare",
        "-Wno-unnamed-type-template-args",
        "-Wno-unused-private-field",
    ],
    include_dirs: ["external/googletest/googletest"],
    static_libs: ["libgtest_ndk_c++"],
    relative_install_path: "gtest_ndk_tests",
    no_named_install_directory: true,
    sdk_version: "9",
    stl: "c++_static",
}

cc_defaults {
    name: "gtest_all_test_defaults",
    srcs: [
        "gtest-*.cc",
        "googletest-*.cc",
    ],
    exclude_srcs: [
        "gtest-unittest-api_test.cc",
        "googletest/src/gtest-all.cc",
        "gtest_all_test.cc",
        "gtest-death-test_ex_test.cc",
        "gtest-listener_test.cc",
        "gtest-unittest-api_test.cc",
        "googletest-param-test-test.cc",
        "googletest-param-test2-test.cc",
        "googletest-catch-exceptions-test_.cc",
        "googletest-color-test_.cc",
        "googletest-env-var-test_.cc",
        "googletest-failfast-unittest_.cc",
        "googletest-filter-unittest_.cc",
        "googletest-global-environment-unittest_.cc",
        "googletest-break-on-failure-unittest_.cc",
        "googletest-listener-test.cc",
        "googletest-message-test.cc",
        "googletest-output-test_.cc",
        "googletest-list-tests-unittest_.cc",
        "googletest-shuffle-test_.cc",
        "googletest-setuptestsuite-test_.cc",
        "googletest-uninitialized-test_.cc",
        "googletest-death-test_ex_test.cc",
        "googletest-param-test-test",
        "googletest-throw-on-failure-test_.cc",
        "googletest-param-test-invalid-name1-test_.cc",
        "googletest-param-test-invalid-name2-test_.cc",
    ],
    cflags: ["-DGTEST_USE_OWN_TR1_TUPLE=1"],
}

cc_test {
    name: "gtest_all_test",
    defaults: [
        "gtest_test_defaults",
        "gtest_all_test_defaults",
    ],
    static_libs: ["libgtest_main"],
}

cc_test {
    name: "gtest_all_test_ndk",
    defaults: [
        "gtest_ndk_test_defaults",
        "gtest_all_test_defaults",
    ],
    static_libs: ["libgtest_main_ndk_c++"],
}

// Tests death tests.
cc_test {
    name: "googletest-death-test-test",
    defaults: ["gtest_test_defaults"],
    srcs: ["googletest-death-test-test.cc"],
    static_libs: ["libgtest_main"],
}

cc_test {
    name: "gtest_test_macro_stack_footprint_test",
    defaults: ["gtest_test_defaults"],
    srcs: ["gtest_test_macro_stack_footprint_test.cc"],
    static_libs: ["libgtest"],
}

//These googletest tests have their own main()
cc_test {
    name: "googletest-listener-test",
    defaults: ["gtest_test_defaults"],
    srcs: ["googletest-listener-test.cc"],
    static_libs: ["libgtest_main"],
}

cc_test {
    name: "googletest-listener-test_ndk",
    defaults: ["gtest_ndk_test_defaults"],
    srcs: ["googletest-listener-test.cc"],
    static_libs: ["libgtest_main_ndk_c++"],
}

cc_test {
    name: "gtest-unittest-api_test",
    defaults: ["gtest_test_defaults"],
    srcs: ["gtest-unittest-api_test.cc"],
    static_libs: ["libgtest"],
}

cc_test {
    name: "gtest-unittest-api_test_ndk",
    defaults: ["gtest_ndk_test_defaults"],
    srcs: ["gtest-unittest-api_test.cc"],
    static_libs: ["libgtest_ndk_c++"],
}

cc_test {
    name: "googletest-param-test-test",
    defaults: ["gtest_test_defaults"],
    srcs: [
        "googletest-param-test-test.cc",
        "googletest-param-test2-test.cc",
    ],
    static_libs: ["libgtest"],
}

cc_test {
    name: "googletest-param-test-test_ndk",
    defaults: ["gtest_ndk_test_defaults"],
    srcs: [
        "googletest-param-test-test.cc",
        "googletest-param-test2-test.cc",
    ],
}

cc_test {
    name: "gtest_unittest",
    defaults: ["gtest_test_defaults"],
    srcs: ["gtest_unittest.cc"],
    static_libs: ["libgtest_main"],
}

cc_test {
    name: "gtest_unittest_ndk",
    defaults: ["gtest_ndk_test_defaults"],
    srcs: ["gtest_unittest.cc"],
    static_libs: ["libgtest_main_ndk_c++"],
}

cc_test {
    name: "gtest-typed-test_test",
    defaults: ["gtest_test_defaults"],
    srcs: [
        "gtest-typed-test_test.cc",
        "gtest-typed-test2_test.cc",
    ],
    static_libs: ["libgtest_main"],
}

cc_test {
    name: "gtest-typed-test_test_ndk",
    defaults: ["gtest_ndk_test_defaults"],
    srcs: [
        "gtest-typed-test_test.cc",
        "gtest-typed-test2_test.cc",
    ],
    static_libs: ["libgtest_main_ndk_c++"],
}

cc_test {
    name: "gtest_prod_test",
    defaults: ["gtest_test_defaults"],
    srcs: [
        "gtest_prod_test.cc",
        "production.cc",
    ],
    static_libs: ["libgtest_main"],
}

cc_test {
    name: "gtest_prod_test_ndk",
    defaults: ["gtest_ndk_test_defaults"],
    srcs: [
        "gtest_prod_test.cc",
        "production.cc",
    ],
    static_libs: ["libgtest_main_ndk_c++"],
}

cc_test {
    name: "gtest_skip_test",
    defaults: ["gtest_test_defaults"],
    srcs: ["gtest_skip_test.cc"],
    static_libs: ["libgtest_main"],
}

cc_test {
    name: "gtest_skip_test_ndk",
    defaults: ["gtest_ndk_test_defaults"],
    srcs: ["gtest_skip_test.cc"],
    static_libs: ["libgtest_main_ndk_c++"],
}

cc_test {
    name: "gtest_skip_in_environment_setup_test",
    defaults: ["gtest_test_defaults"],
    srcs: ["gtest_skip_in_environment_setup_test.cc"],
    static_libs: ["libgtest_main"],
}

cc_test {
    name: "gtest_skip_in_environment_setup_test_ndk",
    defaults: ["gtest_ndk_test_defaults"],
    srcs: ["gtest_skip_in_environment_setup_test.cc"],
    static_libs: ["libgtest_main_ndk_c++"],
}

cc_test {
    name: "gtest_no_test_unittest",
    defaults: ["gtest_test_defaults"],
    srcs: ["gtest_no_test_unittest.cc"],
    static_libs: ["libgtest_main"],
}

cc_test {
    name: "gtest_no_test_unittest_ndk",
    defaults: ["gtest_ndk_test_defaults"],
    srcs: ["gtest_no_test_unittest.cc"],
    static_libs: ["libgtest_main_ndk_c++"],
}

cc_test {
    name: "gtest_environment_test",
    defaults: ["gtest_test_defaults"],
    srcs: ["gtest_environment_test.cc"],
    static_libs: ["libgtest"],
}

cc_test {
    name: "gtest_environment_test_ndk",
    defaults: ["gtest_ndk_test_defaults"],
    srcs: ["gtest_environment_test.cc"],
    static_libs: ["libgtest_ndk_c++"],
}

cc_test {
    name: "gtest_premature_exit_test",
    defaults: ["gtest_test_defaults"],
    srcs: ["gtest_premature_exit_test.cc"],
    static_libs: ["libgtest"],
}

cc_test {
    name: "gtest_premature_exit_test_ndk",
    defaults: ["gtest_ndk_test_defaults"],
    srcs: ["gtest_premature_exit_test.cc"],
    static_libs: ["libgtest_ndk_c++"],
}

cc_test {
    name: "gtest_repeat_test",
    defaults: ["gtest_test_defaults"],
    srcs: ["gtest_repeat_test.cc"],
    static_libs: ["libgtest"],
}

cc_test {
    name: "gtest_repeat_test_ndk",
    defaults: ["gtest_ndk_test_defaults"],
    srcs: ["gtest_repeat_test.cc"],
    static_libs: ["libgtest_ndk_c++"],
}

cc_test {
    name: "gtest_stress_test",
    defaults: ["gtest_test_defaults"],
    srcs: ["gtest_stress_test.cc"],
    static_libs: ["libgtest"],
}

cc_test {
    name: "gtest_stress_test_ndk",
    defaults: ["gtest_ndk_test_defaults"],
    srcs: ["gtest_stress_test.cc"],
    static_libs: ["libgtest_ndk_c++"],
}

cc_test {
    name: "gtest_main_unittest",
    defaults: ["gtest_test_defaults"],
    srcs: ["gtest_main_unittest.cc"],
    static_libs: ["libgtest_main"],
}

cc_test {
    name: "gtest_main_unittest_ndk",
    defaults: ["gtest_ndk_test_defaults"],
    srcs: ["gtest_main_unittest.cc"],
    static_libs: ["libgtest_main_ndk_c++"],
}

cc_test {
    name: "gtest_pred_impl_unittest",
    defaults: ["gtest_test_defaults"],
    srcs: ["gtest_pred_impl_unittest.cc"],
    static_libs: ["libgtest_main"],
}

cc_test {
    name: "gtest_pred_impl_unittest_ndk",
    defaults: ["gtest_ndk_test_defaults"],
    srcs: ["gtest_pred_impl_unittest.cc"],
    static_libs: ["libgtest_main_ndk_c++"],
}

cc_test {
    name: "gtest_sole_header_test",
    defaults: ["gtest_test_defaults"],
    srcs: ["gtest_sole_header_test.cc"],
    static_libs: ["libgtest_main"],
}

cc_test {
    name: "gtest_sole_header_test_ndk",
    defaults: ["gtest_ndk_test_defaults"],
    srcs: ["gtest_sole_header_test.cc"],
    static_libs: ["libgtest_main_ndk_c++"],
}
